package com.abaenglish.moment.repository;

import com.abaenglish.moment.domain.moment.Moment;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

import java.util.List;

@ViewIndexed(designDoc = "moment")
public interface IMomentRepository extends CouchbaseRepository<Moment, String> {

    Moment findOne(String id);

    List<Moment> findAllByTypeOrderByOrderAsc(String type);

}
