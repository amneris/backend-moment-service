package com.abaenglish.moment.repository;

import com.abaenglish.moment.domain.momentprogress.MomentProgress;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbaseRepository;

import java.util.List;

@ViewIndexed(designDoc = "momentProgressResponseRepository")
public interface IMomentProgressRepository extends CouchbaseRepository<MomentProgress, String> {

    MomentProgress findOne(String id);

    Long countByType(String type);

    @Query("SELECT COUNT(*) AS totalMoments FROM moment_service WHERE type = $1")
    Long countAllMoments(String type);

    @Query("#{#n1ql.selectEntity} WHERE type = $1 LIMIT $2 OFFSET $3")
    List<MomentProgress> findAllMoments(String type, Long limit, Long offset);
}
