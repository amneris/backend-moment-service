package com.abaenglish.moment;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MomentApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MomentApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

    }

}
