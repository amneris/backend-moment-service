package com.abaenglish.moment.domain.momentprogress;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Document(expiry = 0)
public class MomentProgress {

    @Id
    private String id;

    @Field
    private String uuid;

    @Field
    private String type;

    @Field
    private String userId;

    @Field
    private List<MomentProgressMoment> moments;

}
