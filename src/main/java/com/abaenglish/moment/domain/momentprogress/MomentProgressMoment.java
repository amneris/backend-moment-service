package com.abaenglish.moment.domain.momentprogress;

import com.couchbase.client.java.repository.annotation.Field;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Document
public class MomentProgressMoment {

    @Field
    private String uuid;

    @Field
    private Integer order;

    @Field
    private Boolean done;

    @Field
    private String addedDate;
}
