package com.abaenglish.moment.domain.moment;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum MomentStatusCategories {

    INACTIVE("Inactive"), ACTIVE("Active"), NEW("New"), DONE("Done");

    private String status;
    private static final Set<MomentStatusCategories> VALUE_LIST = Stream.of(values()).collect(Collectors.toSet());

    MomentStatusCategories(String status) {
        this.status = status;
    }

    public static Optional<MomentStatusCategories> parseStatus(final String status) {
        return VALUE_LIST.stream()
                .filter(e -> e.status.equalsIgnoreCase(status))
                .findFirst();
    }

    public String getStatus() {
        return status;
    }
}
