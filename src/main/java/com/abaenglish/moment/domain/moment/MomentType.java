package com.abaenglish.moment.domain.moment;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum MomentType {

    VOCABULARY("vocabulary"), FILLINTHEGAPS("fill_in_the_gaps"), LISTENINGCOMPREHESION("listening_comprehension");

    private String type;

    private static final Set<MomentType> VALUE_LIST = Stream.of(values()).collect(Collectors.toSet());

    MomentType(String type) {
        this.type = type;
    }

    public static Optional<MomentType> parseType(final String type) {
        return VALUE_LIST.stream()
                .filter(e -> e.type.equalsIgnoreCase(type))
                .findFirst();
    }

    public String getType() {
        return type;
    }

}
