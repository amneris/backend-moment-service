package com.abaenglish.moment.domain.moment;

import com.couchbase.client.java.repository.annotation.Field;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Document
public class Exercise {

    @Field
    private String uuid;
    @Field
    private String type;
    @Field
    private List<Item> items;
    @Field
    private List<Result> results;

}

