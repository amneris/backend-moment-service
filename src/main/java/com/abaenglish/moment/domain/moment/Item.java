package com.abaenglish.moment.domain.moment;

import com.couchbase.client.java.repository.annotation.Field;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Document
public class Item {

    @Field
    private String uuid;
    @Field
    private String type;
    @Field
    private String audio;
    @Field
    private String value;
    @Field
    private String role;

}
