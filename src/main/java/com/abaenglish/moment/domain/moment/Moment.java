package com.abaenglish.moment.domain.moment;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
@Document(expiry = 0)
public class Moment {

    @Id
    private String id;
    @Field
    private String momentType;
    @Field
    private String uuid;
    @Field
    private Integer order;
    @Field
    private String type;
    @Field
    private List<Title> titles;
    @Field
    private String icon;
    @Field
    private String audio;
    @Field
    private List<Exercise> exercises;

}
