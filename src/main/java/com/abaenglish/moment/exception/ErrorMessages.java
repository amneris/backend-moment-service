package com.abaenglish.moment.exception;

import com.abaenglish.boot.exception.CodeMessage;

public enum ErrorMessages {

    MOMENT_NOT_EXIST (new CodeMessage("MOM0001", "Id moment not found in our repository.")),
    USER_NOT_EXIST (new CodeMessage("MOM0002", "User not found.")),
    USER_PROGRESS_NOT_EXIST (new CodeMessage("MOM0003", "User not found in progress."))
    ;

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
