package com.abaenglish.moment.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.exception.ErrorMessages;

public class BadRequestServiceException extends ServiceException {

    public BadRequestServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }
}
