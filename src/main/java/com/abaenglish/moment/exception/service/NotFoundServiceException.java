package com.abaenglish.moment.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.exception.ErrorMessages;

public class NotFoundServiceException extends ServiceException {

    public NotFoundServiceException(ErrorMessages errorMessages) {
        super(errorMessages.getError());
    }

    public NotFoundServiceException(ErrorMessages errorMessages, Throwable cause) {
        super(errorMessages.getError(), cause);
    }

}
