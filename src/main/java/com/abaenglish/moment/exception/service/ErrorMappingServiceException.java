package com.abaenglish.moment.exception.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.exception.ErrorMessages;

public class ErrorMappingServiceException extends ServiceException {

    public ErrorMappingServiceException(ErrorMessages errorMessages, Throwable cause) {
        super(errorMessages.getError(),cause);
    }
}
