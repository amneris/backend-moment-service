package com.abaenglish.moment.controller.v1;

import com.abaenglish.authorization.service.IAuthenticatedUserService;
import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.ForbiddenApiException;
import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.boot.exception.ServerErrorApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.controller.v1.momentprogress.dto.InsertMomentProgressRequest;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressMomentResponse;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressRequest;
import com.abaenglish.moment.exception.service.BadRequestServiceException;
import com.abaenglish.moment.exception.service.NotFoundServiceException;
import com.abaenglish.moment.service.IMomentProgressService;
import com.abaenglish.moment.service.dto.momentprogress.InsertMomentProgressRequestService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressMomentResponseService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1/moments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MomentProgressController {

    @Autowired
    private IAuthenticatedUserService authenticatedUserService;

    @Autowired
    private IMomentProgressService momentProgressService;

    @Autowired
    private OrikaBeanMapper mapper;

    @PreAuthorize("#oauth2.hasScope('write')")
    @RequestMapping(method = RequestMethod.POST, value = "/progress")
    public Object updateMomentProgress(@RequestBody MomentProgressRequest momentProgressRequest) throws ApiException {

        MomentProgressMomentResponseService momentProgressMomentResponseService;

        try {
            momentProgressMomentResponseService = momentProgressService.updateMomentProgress(authenticatedUserService.getUserId(), mapper.map(momentProgressRequest, MomentProgressRequestService.class));
        } catch (NotFoundServiceException e) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (BadRequestServiceException e) {
            throw new NotFoundApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }

        return mapper.map(momentProgressMomentResponseService, MomentProgressMomentResponse.class);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(method = RequestMethod.GET, value = "/progress")
    public Object getMomentProgress(@RequestParam(value = "expand", defaultValue = "", required = false) String expand) throws ApiException {

        List<MomentProgressMomentResponseService> momentProgressMomentResponseService;

        try {
            momentProgressMomentResponseService = momentProgressService.getMomentProgressByUserUuid(authenticatedUserService.getUserId(), Optional.of(expand));
        } catch (NotFoundServiceException e) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }

        return mapper.mapAsList(momentProgressMomentResponseService, MomentProgressMomentResponse.class);
    }

    @PreAuthorize("#oauth2.hasScope('trust')")
    @RequestMapping(method = RequestMethod.POST, value = "/insertprogress")
    public void insertMomentProgress(@RequestBody InsertMomentProgressRequest insertMomentProgressRequest) throws ApiException {

        try {
            momentProgressService.insertMomentProgress(mapper.map(insertMomentProgressRequest, InsertMomentProgressRequestService.class));
        } catch (NotFoundServiceException e) {
            throw new ForbiddenApiException(e);
        } catch (BadRequestServiceException e) {
            throw new NotFoundApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }
    }

}
