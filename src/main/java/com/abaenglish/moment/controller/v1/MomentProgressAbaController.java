package com.abaenglish.moment.controller.v1;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.boot.exception.ServerErrorApiException;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressMomentResponse;
import com.abaenglish.moment.exception.service.NotFoundServiceException;
import com.abaenglish.moment.service.IMomentProgressService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressMomentResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/vabawebapps/moments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MomentProgressAbaController {

    @Autowired
    private IMomentProgressService momentProgressService;

    @Autowired
    private OrikaBeanMapper mapper;

    @Deprecated
    @RequestMapping(method = RequestMethod.GET, value = "/progress/{uuid}")
    public Object getMomentProgress(@PathVariable("uuid") Long uuid,
                                    @RequestParam(value = "expand", defaultValue = "", required = false) String expand) throws ApiException {

        List<MomentProgressMomentResponseService> momentProgressMomentResponseService;

        try {
            momentProgressMomentResponseService = momentProgressService.getMomentProgressByUserUuid(uuid, Optional.of(expand));
        } catch (NotFoundServiceException e) {
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }

        return mapper.mapAsList(momentProgressMomentResponseService, MomentProgressMomentResponse.class);
    }

}
