package com.abaenglish.moment.controller.v1.moment.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class ResultResponse {
    private String id;

}
