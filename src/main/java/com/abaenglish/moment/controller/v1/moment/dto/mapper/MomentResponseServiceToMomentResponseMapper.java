package com.abaenglish.moment.controller.v1.moment.dto.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.moment.controller.v1.moment.dto.MomentResponse;
import com.abaenglish.moment.service.dto.moment.MomentResponseService;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class MomentResponseServiceToMomentResponseMapper extends BaseCustomMapper<MomentResponseService, MomentResponse> {

    public MomentResponseServiceToMomentResponseMapper() {
        super();
    }

    @Override
    public void mapAtoB(MomentResponseService momentResponseService, MomentResponse momentResponse, MappingContext context) {
        super.mapAtoB(momentResponseService, momentResponse, context);

        momentResponse.setId(momentResponseService.getUuid());

        if (momentResponseService.getExercises() != null) {
            for (int cntExercises = 0; cntExercises < momentResponseService.getExercises().size(); cntExercises++) {
                momentResponse.getExercises().get(cntExercises).setId(momentResponseService.getExercises().get(cntExercises).getUuid());
                for (int cntItems = 0; cntItems < momentResponseService.getExercises().get(cntExercises).getItems().size(); cntItems++) {
                    momentResponse.getExercises().get(cntExercises).getItems().get(cntItems).setId(momentResponseService.getExercises().get(cntExercises).getItems().get(cntItems).getUuid());
                }
                if (momentResponseService.getExercises().get(cntExercises).getResults() != null) {
                    for (int cntResults = 0; cntResults < momentResponseService.getExercises().get(cntExercises).getResults().size(); cntResults++) {
                        momentResponse.getExercises().get(cntExercises).getResults().get(cntResults).setId(momentResponseService.getExercises().get(cntExercises).getResults().get(cntResults).getUuid());
                    }
                }

            }
        }

    }
}
