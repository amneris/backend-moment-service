package com.abaenglish.moment.controller.v1.moment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class ExerciseResponse {

    private String id;
    private String type;
    private List<ItemResponse> items = null;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ResultResponse> results = null;
}
