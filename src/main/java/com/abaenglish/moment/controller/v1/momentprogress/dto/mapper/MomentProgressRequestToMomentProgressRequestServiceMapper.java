package com.abaenglish.moment.controller.v1.momentprogress.dto.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressRequest;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressRequestService;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class MomentProgressRequestToMomentProgressRequestServiceMapper extends BaseCustomMapper<MomentProgressRequest, MomentProgressRequestService> {

    public MomentProgressRequestToMomentProgressRequestServiceMapper() {
        super();
        addField("id", "momentUuid");
    }

    @Override
    public void mapAtoB(MomentProgressRequest momentProgressRequest, MomentProgressRequestService momentProgressRequestService, MappingContext context) {
        super.mapAtoB(momentProgressRequest, momentProgressRequestService, context);
    }

}