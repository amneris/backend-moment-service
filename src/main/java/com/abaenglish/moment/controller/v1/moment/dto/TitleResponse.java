package com.abaenglish.moment.controller.v1.moment.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
public class TitleResponse {

    private String language;
    private String name;

}
