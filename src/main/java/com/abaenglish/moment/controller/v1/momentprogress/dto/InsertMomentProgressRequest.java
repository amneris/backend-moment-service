package com.abaenglish.moment.controller.v1.momentprogress.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class InsertMomentProgressRequest {

    private Integer order;

    private Long limit;
}
