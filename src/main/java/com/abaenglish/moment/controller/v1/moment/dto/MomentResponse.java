package com.abaenglish.moment.controller.v1.moment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class MomentResponse {

    private String id;
    private String momentType;
    private List<TitleResponse> titles;
    private String icon;
    private String audio;
    private String status;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ExerciseResponse> exercises;
}
