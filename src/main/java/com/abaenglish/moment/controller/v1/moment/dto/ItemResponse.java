package com.abaenglish.moment.controller.v1.moment.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class ItemResponse {

    private String id;
    private String type;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String audio;
    private String value;
    private String role;
}
