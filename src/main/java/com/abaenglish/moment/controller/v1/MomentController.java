package com.abaenglish.moment.controller.v1;

import com.abaenglish.authorization.service.IAuthenticatedUserService;
import com.abaenglish.boot.exception.*;
import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.controller.v1.moment.dto.MomentResponse;
import com.abaenglish.moment.exception.service.BadRequestServiceException;
import com.abaenglish.moment.exception.service.NotFoundServiceException;
import com.abaenglish.moment.service.IMomentService;
import com.abaenglish.moment.service.dto.moment.MomentResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "api/v1/moments", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MomentController {

    @Autowired
    private IAuthenticatedUserService authenticatedUserService;

    @Autowired
    private IMomentService momentService;

    @Autowired
    private OrikaBeanMapper mapper;

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(method = RequestMethod.GET)
    public List<MomentResponse> getMoments(@RequestParam(value = "expand", defaultValue = "", required = false) String expand) throws ApiException {

        List<MomentResponseService> momentResponseServices;

        try {
            momentResponseServices = momentService.getMoments(authenticatedUserService.getUserId(), Optional.of(expand));
        } catch (BadRequestServiceException e) {
            throw new BadRequestApiException(e);
        } catch (NotFoundServiceException e) {
            throw new NotFoundApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }

        return mapper.mapAsList(momentResponseServices, MomentResponse.class);
    }

    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping(method = RequestMethod.GET, value = "/{uuid}")
    public MomentResponse getMoment(@PathVariable("uuid") String uuid,
                                    @RequestParam(value = "expand", defaultValue = "", required = false) String expand) throws ApiException {

        MomentResponseService momentResponseService;
        try {
            momentResponseService = momentService.getMomentByUuid(authenticatedUserService.getUserId(), uuid, Optional.of(expand));
        } catch (BadRequestServiceException e) {
            throw new BadRequestApiException(e);
        } catch (NotFoundServiceException e) {
            throw new ForbiddenApiException(e);
        } catch (ServiceException e) {
            throw new ServerErrorApiException(e);
        }

        return mapper.map(momentResponseService, MomentResponse.class);
    }

}
