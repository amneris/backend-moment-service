package com.abaenglish.moment.controller.v1.momentprogress.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MomentProgressMomentResponse {

    private String id;

    private Integer order;

    private String addedDate;
}
