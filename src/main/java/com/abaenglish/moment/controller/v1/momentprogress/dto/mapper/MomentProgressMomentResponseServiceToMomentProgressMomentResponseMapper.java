package com.abaenglish.moment.controller.v1.momentprogress.dto.mapper;

import com.abaenglish.boot.orika.BaseCustomMapper;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressMomentResponse;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressMomentResponseService;
import ma.glasnost.orika.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class MomentProgressMomentResponseServiceToMomentProgressMomentResponseMapper extends BaseCustomMapper<MomentProgressMomentResponseService, MomentProgressMomentResponse> {

    public MomentProgressMomentResponseServiceToMomentProgressMomentResponseMapper() {
        super();
        addField("uuid", "id");
    }

    @Override
    public void mapAtoB(MomentProgressMomentResponseService momentProgressMomentResponseService, MomentProgressMomentResponse momentProgressMomentResponse, MappingContext context) {
        super.mapAtoB(momentProgressMomentResponseService, momentProgressMomentResponse, context);
    }

}