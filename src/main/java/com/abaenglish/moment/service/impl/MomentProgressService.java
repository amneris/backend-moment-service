package com.abaenglish.moment.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.domain.moment.Moment;
import com.abaenglish.moment.domain.momentprogress.MomentProgress;
import com.abaenglish.moment.domain.momentprogress.MomentProgressMoment;
import com.abaenglish.moment.exception.ErrorMessages;
import com.abaenglish.moment.exception.service.BadRequestServiceException;
import com.abaenglish.moment.exception.service.NotFoundServiceException;
import com.abaenglish.moment.repository.IMomentProgressRepository;
import com.abaenglish.moment.repository.IMomentRepository;
import com.abaenglish.moment.service.IMomentProgressService;
import com.abaenglish.moment.service.dto.momentprogress.InsertMomentProgressRequestService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressMomentResponseService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MomentProgressService implements IMomentProgressService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MomentProgressService.class);
    private static final String EXPAND_ALL = "all";
    private static final String DOCUMENT_TYPE = "moment_progress";
    private final OrikaBeanMapper mapper;
    private final IMomentProgressRepository momentProgressRepository;
    private final IMomentRepository momentRepository;

    @Autowired
    public MomentProgressService(OrikaBeanMapper mapper, IMomentProgressRepository momentProgressRepository, IMomentRepository momentRepository) {
        this.mapper = mapper;
        this.momentProgressRepository = momentProgressRepository;
        this.momentRepository = momentRepository;
    }

    @Override
    public MomentProgressMomentResponseService updateMomentProgress(Long userId, MomentProgressRequestService momentProgressRequestService) throws ServiceException {

        MomentProgress momentProgressResponseRepository = momentProgressRepository.findOne(DOCUMENT_TYPE + "_" + userId.toString());

        if (momentProgressResponseRepository == null) {
            LOGGER.info("Can not find '{}' user progress by id '{}'", userId.toString(), momentProgressRequestService.getMomentUuid());
            throw new NotFoundServiceException(ErrorMessages.USER_NOT_EXIST);
        }

        List<MomentProgressMoment> momentsList = momentProgressResponseRepository.getMoments();

        if (!momentsList.stream()
                .anyMatch(moment -> moment.getUuid().equals(momentProgressRequestService.getMomentUuid()))
                ) {
            LOGGER.info("Moment progress with id '{}' already exists", momentProgressRequestService.getMomentUuid());
            throw new BadRequestServiceException(ErrorMessages.MOMENT_NOT_EXIST);
        }

        momentsList.stream()
                .filter(moment -> moment.getUuid().equals(momentProgressRequestService.getMomentUuid()))
                .forEach(doneMoment -> doneMoment.setDone(true))
        ;

        momentProgressRepository.save(momentProgressResponseRepository);

        return mapper.map(momentsList.stream()
                        .filter(moment -> moment.getDone().equals(true))
                        .filter(moment -> moment.getUuid().equals(momentProgressRequestService.getMomentUuid()))
                        .findFirst().get()
                , MomentProgressMomentResponseService.class);

    }

    @Override
    public List<MomentProgressMomentResponseService> getMomentProgressByUserUuid(Long userId, Optional<String> expand) throws ServiceException {

        List<String> expandItems = expand.isPresent() ? Arrays.asList(expand.get().split(",")) : new ArrayList<>();

        MomentProgress momentProgressResponseRepository = momentProgressRepository.findOne(DOCUMENT_TYPE + "_" + userId.toString());

        if (momentProgressResponseRepository == null) {

            LOGGER.info("Can not find '{}' user", userId);
            throw new NotFoundServiceException(ErrorMessages.USER_NOT_EXIST);
        }

        List<MomentProgressMoment> momentsList = momentProgressResponseRepository.getMoments();

        Integer limit = expandItems.contains(EXPAND_ALL) ? Integer.MAX_VALUE : 1;

        return momentsList.stream()
                .sorted((order1, order2) -> order2.getOrder()
                        .compareTo(order1.getOrder()))
                .limit(limit)
                .filter(moment -> moment.getDone().equals(false))
                .map(moment -> mapper.map(moment, MomentProgressMomentResponseService.class))
                .collect(Collectors.toList());

    }

    private List<MomentProgress> filterMomentProgress(List<MomentProgress> momentProgressList, String uuid) {

        List<MomentProgress> momentProgressListResults = new ArrayList<MomentProgress>();

        for (MomentProgress momentProgress : momentProgressList) {

            if (!momentProgress.getMoments().stream()
                    .anyMatch(moment -> moment.getUuid().equals(uuid))) {

                momentProgressListResults.add(momentProgress);
            }
        }

        return momentProgressListResults;
    }

    @Override
    public Boolean insertMomentProgress(InsertMomentProgressRequestService insertMomentProgressRequestService) throws ServiceException {

        //@TODO  - HashMap

        List<Moment> moments = momentRepository.findAllByTypeOrderByOrderAsc("moment");

        if (moments == null) {
            LOGGER.info("Can not find moments");
            throw new NotFoundServiceException(ErrorMessages.MOMENT_NOT_EXIST);
        }


        // Check if not exists aba moment with order

        if (!moments.stream()
                .anyMatch(moment -> moment.getOrder().equals(insertMomentProgressRequestService.getOrder()))
                ) {
            LOGGER.info("Moment progress with order '{}' already exists", insertMomentProgressRequestService.getOrder());
            throw new BadRequestServiceException(ErrorMessages.MOMENT_NOT_EXIST);
        }


        // Create NEXT momentProgress for inject

        MomentProgressMoment momentProgressMoment = new MomentProgressMoment();

        momentProgressMoment.setAddedDate(LocalDate.now().toString());
        momentProgressMoment.setDone(false);
        momentProgressMoment.setOrder(insertMomentProgressRequestService.getOrder());
        momentProgressMoment.setUuid(moments.stream()
                .filter(moment -> moment.getOrder().equals(insertMomentProgressRequestService.getOrder()))
                .findFirst().get().getUuid());


        Long totalMoments = momentProgressRepository.countAllMoments("moment_progress");

        for (Long offset = 0L; offset < totalMoments.longValue(); offset += insertMomentProgressRequestService.getLimit().longValue()) {

            List<MomentProgress> momentProgress = this.filterMomentProgress(momentProgressRepository.findAllMoments("moment_progress", insertMomentProgressRequestService.getLimit().longValue(), offset), momentProgressMoment.getUuid());

            momentProgress.stream()
                    .forEach(progress -> progress.getMoments().add(momentProgressMoment))
            ;

            momentProgressRepository.save(momentProgress);
        }

        return true;
    }

}
