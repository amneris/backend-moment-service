package com.abaenglish.moment.service.impl;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.domain.moment.Moment;
import com.abaenglish.moment.domain.moment.MomentStatusCategories;
import com.abaenglish.moment.domain.momentprogress.MomentProgress;
import com.abaenglish.moment.domain.momentprogress.MomentProgressMoment;
import com.abaenglish.moment.exception.ErrorMessages;
import com.abaenglish.moment.exception.service.BadRequestServiceException;
import com.abaenglish.moment.exception.service.NotFoundServiceException;
import com.abaenglish.moment.repository.IMomentProgressRepository;
import com.abaenglish.moment.repository.IMomentRepository;
import com.abaenglish.moment.service.IMomentService;
import com.abaenglish.moment.service.dto.moment.MomentResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class MomentService implements IMomentService {

    private static final String WITH_EXERCISES = "exercises";
    private static final String WITH_RESULTS = "results";
    private static final String DOCUMENT_TYPE_MOMENT = "moment";
    private static final String DOCUMENT_TYPE_MOMENTPROGRESS = "moment_progress";

    private final OrikaBeanMapper mapper;
    private final IMomentRepository momentRepository;
    private final IMomentProgressRepository momentProgressRepository;

    @Autowired
    public MomentService(OrikaBeanMapper mapper, IMomentRepository momentRepository, IMomentProgressRepository momentProgressRepository) {
        this.mapper = mapper;
        this.momentRepository = momentRepository;
        this.momentProgressRepository = momentProgressRepository;
    }

    @Override
    public List<MomentResponseService> getMoments(Long userId, Optional<String> expand) throws ServiceException {

        MomentProgress momentProgress = momentProgressRepository.findOne(DOCUMENT_TYPE_MOMENTPROGRESS + "_" + userId.toString());

        if (momentProgress == null) {
            throw new NotFoundServiceException(ErrorMessages.USER_PROGRESS_NOT_EXIST);
        }

        List<String> expandItems = expand.isPresent() ? Arrays.asList(expand.get().split(",")) : new ArrayList<>();

        List<Moment> moment = momentRepository.findAllByTypeOrderByOrderAsc("moment");
        List<MomentResponseService> momentResponseServices = mapper.mapAsList(moment, MomentResponseService.class);

        if (!expandItems.contains(WITH_EXERCISES)) {
            momentResponseServices.stream().forEach(m -> m.setExercises(null));
        } else if (!expandItems.contains(WITH_RESULTS)) {
            momentResponseServices.stream().forEach(m -> m.getExercises().stream().forEach(e -> e.setResults(null)));
        }

        List<MomentResponseService> momentResponseServicesResult = getListUpdateStatusByUser(momentProgress, momentResponseServices);

        MomentResponseService lastMoment = momentResponseServicesResult.stream()
                .filter(mt -> mt.getStatus() != MomentStatusCategories.INACTIVE.getStatus())
                .reduce((first, second) -> second).get();

        if(lastMoment.getStatus() == MomentStatusCategories.ACTIVE.getStatus()) {
            lastMoment.setStatus(MomentStatusCategories.NEW.getStatus());
        }

        return momentResponseServicesResult;

    }

    @Override
    public MomentResponseService getMomentByUuid(Long userId, String momentUuid, Optional<String> expand) throws ServiceException {

        MomentProgress momentProgress = momentProgressRepository.findOne(DOCUMENT_TYPE_MOMENTPROGRESS + "_" + userId.toString());

        if (momentProgress == null) {
            throw new NotFoundServiceException(ErrorMessages.USER_PROGRESS_NOT_EXIST);
        }

        List<String> expandItems = expand.isPresent() ? Arrays.asList(expand.get().split(",")) : new ArrayList<>();

        Moment moment = momentRepository.findOne(DOCUMENT_TYPE_MOMENT + "_" + momentUuid);

        if (moment == null) {
            throw new BadRequestServiceException(ErrorMessages.MOMENT_NOT_EXIST);
        }

        MomentResponseService momentResponseService = mapper.map(moment, MomentResponseService.class);

        if (!expandItems.contains("exercises")) {
            momentResponseService.setExercises(null);
        } else if (!expandItems.contains("results")) {
            momentResponseService.getExercises().stream().forEach(e -> e.setResults(null));
        }

        return getUpdateStatusByUser(momentProgress, momentResponseService);

    }

    private List<MomentResponseService> getListUpdateStatusByUser(MomentProgress momentProgress, List<MomentResponseService> momentResponseServices) {

        List<MomentProgressMoment> momentsList = momentProgress.getMoments();

        momentResponseServices.stream().forEach(mrs -> mrs.setStatus(getStatusFromUuid(momentsList, mrs.getUuid())));

        return momentResponseServices;
    }

    private MomentResponseService getUpdateStatusByUser(MomentProgress momentProgress, MomentResponseService momentResponseService) {

        List<MomentProgressMoment> momentsList = momentProgress.getMoments();

        momentResponseService.setStatus(getStatusFromUuid(momentsList, momentResponseService.getUuid()));

        return momentResponseService;
    }

    private String getStatusFromUuid(List<MomentProgressMoment> momentsList, String uuid) {

        String status;

        Optional<MomentProgressMoment> momentProgressMoment = momentsList.stream()
                .filter(moment -> moment.getUuid().equals(uuid))
                .findFirst();

        if (!momentProgressMoment.isPresent()) {
            status = MomentStatusCategories.INACTIVE.getStatus();
        } else if (momentProgressMoment.get().getDone()) {
            status = MomentStatusCategories.DONE.getStatus();
        } else {
            status = MomentStatusCategories.ACTIVE.getStatus();
        }

        return status;
    }

}
