package com.abaenglish.moment.service.dto.moment;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MomentResponseService {

    private String uuid;
    private String momentType;
    private List<TitleResponseService> titles;
    private String icon;
    private String audio;
    private String status;
    private List<ExerciseResponseService> exercises;

    public MomentResponseService() {
        //This method is empty because Orika needs it and Lombok overwrite the creation.
    }
}
