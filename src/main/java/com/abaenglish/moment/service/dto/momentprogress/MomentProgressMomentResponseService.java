package com.abaenglish.moment.service.dto.momentprogress;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MomentProgressMomentResponseService {

    private String uuid;

    private Integer order;

    private Boolean done;

    private String addedDate;

}
