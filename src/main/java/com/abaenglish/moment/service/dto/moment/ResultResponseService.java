package com.abaenglish.moment.service.dto.moment;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ResultResponseService {

    private String uuid;

}
