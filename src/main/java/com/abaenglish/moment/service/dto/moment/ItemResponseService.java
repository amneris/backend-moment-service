package com.abaenglish.moment.service.dto.moment;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ItemResponseService {

    private String uuid;
    private String type;
    private String audio;
    private String value;
    private String role;
}
