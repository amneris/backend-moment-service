package com.abaenglish.moment.service.dto.momentprogress;

import com.abaenglish.moment.domain.momentprogress.MomentProgressMoment;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MomentProgressResponseService {

    private String uuid;

    private String userId;

    private List<MomentProgressMoment> moments;

}
