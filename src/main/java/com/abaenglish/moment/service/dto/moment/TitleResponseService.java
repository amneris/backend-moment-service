package com.abaenglish.moment.service.dto.moment;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor
public class TitleResponseService {

    private String language;
    private String name;

}
