package com.abaenglish.moment.service.dto.momentprogress;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class MomentProgressRequestService {

    private String momentUuid;
}
