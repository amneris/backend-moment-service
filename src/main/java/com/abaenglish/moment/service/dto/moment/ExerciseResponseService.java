package com.abaenglish.moment.service.dto.moment;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ExerciseResponseService {

    private String uuid;
    private String type;
    private List<ItemResponseService> items;
    private List<ResultResponseService> results;
}
