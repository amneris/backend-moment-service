package com.abaenglish.moment.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.service.dto.moment.MomentResponseService;

import java.util.List;
import java.util.Optional;

public interface IMomentService {

    List<MomentResponseService> getMoments(Long userId, Optional<String> expand) throws ServiceException;

    MomentResponseService getMomentByUuid(Long userId, String momentUuid, Optional<String> expand) throws ServiceException;

}
