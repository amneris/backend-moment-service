package com.abaenglish.moment.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.service.dto.momentprogress.InsertMomentProgressRequestService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressMomentResponseService;
import com.abaenglish.moment.service.dto.momentprogress.MomentProgressRequestService;

import java.util.List;
import java.util.Optional;

public interface IMomentProgressService {

    MomentProgressMomentResponseService updateMomentProgress(Long userId, MomentProgressRequestService momentProgressRequestService) throws ServiceException;

    List<MomentProgressMomentResponseService> getMomentProgressByUserUuid(Long userId, Optional<String> expand) throws ServiceException;

    Boolean insertMomentProgress(InsertMomentProgressRequestService insertMomentProgressRequestService) throws ServiceException;

}
