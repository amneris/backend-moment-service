package com.abaenglish.moment.service;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.boot.orika.OrikaBeanMapper;
import com.abaenglish.moment.repository.IMomentProgressRepository;
import com.abaenglish.moment.repository.IMomentRepository;
import com.abaenglish.moment.service.impl.MomentService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class MomentServiceTest {

    public static final String UID_PARTS_OF_THE_BODY = "14c2d019-fabb-4fd8-97cd-a96046cab83f";
    @Autowired
    private OrikaBeanMapper mapper = new OrikaBeanMapper(true);
    private IMomentService momentService;
    private IMomentRepository momentRepository;
    private IMomentProgressRepository momentProgressRepository;

    @Before
    public void setUp() {

        momentService = new MomentService(mapper, momentRepository, momentProgressRepository);
    }

    @Test
    @Ignore
    public void testGetMomentsWithoutExercises() throws ServiceException {

    }

}
