package com.abaenglish.moment.controller.v1;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressMomentResponse;
import com.abaenglish.moment.domain.momentprogress.MomentProgress;
import com.abaenglish.moment.objectmother.MomentProgressObjectMother;
import com.abaenglish.moment.repository.IMomentProgressRepository;
import com.abaenglish.moment.service.IMomentProgressService;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MomentProgressControllerIT {


    @SpyBean
    IMomentProgressService momentProgressService;

    @MockBean
    IMomentProgressRepository momentProgressRepository;

    @Value("${local.server.port}")
    int port;

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @After
    public void tearDown() {
        RestAssured.reset();
    }


    @Ignore
    @Test
    public void getMomentProgressWithoutExpand() throws ServiceException {

    }

    @Ignore
    @Test
    public void getMomentProgressExpandAll() throws ServiceException {

        // controller
        List<MomentProgressMomentResponse> momentProgressMomentResponse = MomentProgressObjectMother.buildMomentProgressResponse();

        MomentProgress momentProgress = MomentProgressObjectMother.buildMomentProgress();

        // repository
        when(momentProgressRepository.findOne(Mockito.anyString())).thenReturn(momentProgress);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);


        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                get("/api/v1/momentprogress/10000063/?expand=all").
                then().
                spec(responseSpec);

        verify(momentProgressService).getMomentProgressByUserUuid(Mockito.anyLong(), any());

    }

}
