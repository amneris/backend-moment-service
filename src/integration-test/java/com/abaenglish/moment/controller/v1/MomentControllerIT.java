package com.abaenglish.moment.controller.v1;

import com.abaenglish.boot.exception.service.ServiceException;
import com.abaenglish.moment.controller.v1.moment.dto.*;
import com.abaenglish.moment.domain.moment.Moment;
import com.abaenglish.moment.objectmother.MomentObjectMother;
import com.abaenglish.moment.repository.IMomentRepository;
import com.abaenglish.moment.service.IMomentService;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static io.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MomentControllerIT {

    public static final String UID_PARTS_OF_THE_BODY = "14c2d019-fabb-4fd8-97cd-a96046cab83f";

    @SpyBean
    IMomentService momentService;

    @MockBean
    IMomentRepository momentRepository;

    @Value("${local.server.port}")
    int port;

    @After
    public void tearDown() {
        RestAssured.reset();
    }

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Ignore
    @Test
    public void getMomentWithoutExercises() throws ServiceException {

        List<MomentResponse> momentResponses = MomentObjectMother.buildMomentResponseListWithoutExercises();
        Moment moment = MomentObjectMother.buildMomentWithoutExercises();

        when(momentRepository.findOne(Mockito.anyString())).thenReturn(moment);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);

        Integer cntMoment = 0;
        Integer cntTitles = 0;

        for (MomentResponse momentResponse : momentResponses) {
            builder.expectBody("id", is(momentResponse.getId()));
            builder.expectBody("icon", is(momentResponse.getIcon()));
            builder.expectBody("audio", is(momentResponse.getAudio()));
            builder.expectBody("exercises", is(nullValue()));
            cntTitles = 0;
            for (TitleResponse titleResponse : momentResponse.getTitles()) {
                builder.expectBody("titles[" + cntTitles.toString() + "].language", is(titleResponse.getLanguage()));
                builder.expectBody("titles[" + cntTitles.toString() + "].name", is(titleResponse.getName()));
                cntTitles++;
            }
            cntMoment++;
        }

        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                get("/api/v1/moments/" + UID_PARTS_OF_THE_BODY).
                then().
                spec(responseSpec);

        verify(momentService).getMomentByUuid(Mockito.anyLong(), Mockito.anyString(), any());

    }

    @Ignore
    @Test
    public void getMomentWithExercises() throws ServiceException {

        List<MomentResponse> momentResponses = MomentObjectMother.buildMomentResponsesWithExercises();
        Moment moment = MomentObjectMother.buildMomentWithExercises();

        when(momentRepository.findOne(Mockito.anyString())).thenReturn(moment);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);

        Integer cntMoment = 0;
        Integer cntExercise;
        Integer cntItem;
        Integer cntResult;
        Integer cntTitles;

        for (MomentResponse momentResponse : momentResponses) {
            builder.expectBody("id", is(momentResponse.getId()));
            builder.expectBody("icon", is(momentResponse.getIcon()));
            builder.expectBody("audio", is(momentResponse.getAudio()));
            cntTitles = 0;
            for (TitleResponse titleResponse : momentResponse.getTitles()) {
                builder.expectBody("titles[" + cntTitles.toString() + "].language", is(titleResponse.getLanguage()));
                builder.expectBody("titles[" + cntTitles.toString() + "].name", is(titleResponse.getName()));
                cntTitles++;
            }
//            builder.expectBody("status", is(momentResponse.getStatus()));
            cntExercise = 0;
            for (ExerciseResponse exerciseResponse : momentResponse.getExercises()) {
                builder.expectBody("exercises[" + cntExercise.toString() + "].id", is(exerciseResponse.getId()));
                builder.expectBody("exercises[" + cntExercise.toString() + "].type", is(exerciseResponse.getType()));
                cntItem = 0;
                for (ItemResponse itemResponse : exerciseResponse.getItems()) {
                    builder.expectBody("exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].id", is(itemResponse.getId()));
                    builder.expectBody("exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].type", is(itemResponse.getType()));
                    builder.expectBody("exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].audio", is(itemResponse.getAudio()));
                    builder.expectBody("exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].value", is(itemResponse.getValue()));
                    builder.expectBody("exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].role", is(itemResponse.getRole()));
                    cntItem++;
                }
                cntResult = 0;
                for (ResultResponse resultResponse : exerciseResponse.getResults()) {
                    builder.expectBody("exercises[" + cntExercise.toString() + "].results[" + cntResult.toString() + "].id", is(resultResponse.getId()));
                    cntResult++;
                }
                cntExercise++;
            }
            cntMoment++;
        }

        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                param("expand", "exercises,results").
                get("/api/v1/moments/" + UID_PARTS_OF_THE_BODY).
                then().
                spec(responseSpec);

        verify(momentService).getMomentByUuid(Mockito.anyLong(), Mockito.anyString(), any());

    }

    @Ignore
    @Test
    public void getMomentsWithExercises() throws ServiceException {

        List<MomentResponse> momentResponses = MomentObjectMother.buildMomentResponsesWithExercises();
        List<Moment> momentResponseRepositories = MomentObjectMother.buildMomentResponsesRepositoryWithExercises();

        when(momentRepository.findAllByTypeOrderByOrderAsc("moment")).thenReturn(momentResponseRepositories);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);

        Integer cntMoment = 0;
        Integer cntExercise;
        Integer cntItem;
        Integer cntResult;
        Integer cntTitles;

        for (MomentResponse momentResponse : momentResponses) {
            builder.expectBody("[" + cntMoment.toString() + "].id", is(momentResponse.getId()));
            builder.expectBody("[" + cntMoment.toString() + "].icon", is(momentResponse.getIcon()));
            builder.expectBody("[" + cntMoment.toString() + "].audio", is(momentResponse.getAudio()));
            cntTitles = 0;
            for (TitleResponse titleResponse : momentResponse.getTitles()) {
                builder.expectBody("[" + cntMoment.toString() + "].titles[" + cntTitles.toString() + "].language", is(titleResponse.getLanguage()));
                builder.expectBody("[" + cntMoment.toString() + "].titles[" + cntTitles.toString() + "].name", is(titleResponse.getName()));
                cntTitles++;
            }
//            builder.expectBody("[" + cntMoment.toString() + "].status", is(momentResponse.getStatus()));
            cntExercise = 0;
            for (ExerciseResponse exerciseResponse : momentResponse.getExercises()) {
                builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].id", is(exerciseResponse.getId()));
                builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].type", is(exerciseResponse.getType()));
                cntItem = 0;
                for (ItemResponse itemResponse : exerciseResponse.getItems()) {
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].id", is(itemResponse.getId()));
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].type", is(itemResponse.getType()));
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].audio", is(itemResponse.getAudio()));
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].value", is(itemResponse.getValue()));
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].items[" + cntItem.toString() + "].role", is(itemResponse.getRole()));
                    cntItem++;
                }
                cntResult = 0;
                for (ResultResponse resultResponse : exerciseResponse.getResults()) {
                    builder.expectBody("[" + cntMoment.toString() + "].exercises[" + cntExercise.toString() + "].results[" + cntResult.toString() + "].id", is(resultResponse.getId()));
                    cntResult++;
                }
                cntExercise++;
            }
            cntMoment++;
        }

        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                param("expand", "exercises,results").
                get("/api/v1/moments").
                then().
                spec(responseSpec);

        verify(momentService).getMoments(Mockito.anyLong(), Mockito.any());

    }

    @Ignore
    @Test
    public void getMomentsWithoutExercises() throws ServiceException {

        List<MomentResponse> momentResponses = MomentObjectMother.buildMomentResponseListWithoutExercises();
        List<Moment> momentResponseRepositories = MomentObjectMother.buildMomentResponsesRepositoryWithoutExercises();

        when(momentRepository.findAllByTypeOrderByOrderAsc("moment")).thenReturn(momentResponseRepositories);

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(200);

        Integer cntMoment = 0;
        Integer cntExercise;
        Integer cntItem;
        Integer cntResult;
        Integer cntTitles;

        for (MomentResponse momentResponse : momentResponses) {
            builder.expectBody("[" + cntMoment.toString() + "].id", is(momentResponse.getId()));
            builder.expectBody("[" + cntMoment.toString() + "].icon", is(momentResponse.getIcon()));
            builder.expectBody("[" + cntMoment.toString() + "].audio", is(momentResponse.getAudio()));
            builder.expectBody("[" + cntMoment.toString() + "].exercises", is(nullValue()));
            cntTitles = 0;
            for (TitleResponse titleResponse : momentResponse.getTitles()) {
                builder.expectBody("[" + cntMoment.toString() + "].titles[" + cntTitles.toString() + "].language", is(titleResponse.getLanguage()));
                builder.expectBody("[" + cntMoment.toString() + "].titles[" + cntTitles.toString() + "].name", is(titleResponse.getName()));
                cntTitles++;
            }
//            builder.expectBody("[" + cntMoment.toString() + "].status", is(momentResponse.getStatus()));
            cntMoment++;
        }

        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                get("/api/v1/moments").
                then().
                spec(responseSpec);

        verify(momentService).getMoments(Mockito.anyLong(), Mockito.any());

    }

    @Ignore
    @Test
    public void forceBadExercisesResponse() throws ServiceException {

        final String id = "999";

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        builder.expectStatusCode(400);
        builder.expectBody("status", is(400));
        builder.expectBody("message", is("Id moment not found in our repository."));
        builder.expectBody("abaCode", is("MOM0005"));

        ResponseSpecification responseSpec = builder.build();

        given().
                contentType(ContentType.JSON).
                when().
                get(format("/api/v1/moments/%s", id)).
                then().
                spec(responseSpec);

    }

}
