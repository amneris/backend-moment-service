package com.abaenglish.moment.objectmother;

import com.abaenglish.moment.controller.v1.moment.dto.*;
import com.abaenglish.moment.domain.moment.*;

import java.util.ArrayList;
import java.util.List;

public class MomentObjectMother {

    public static List<MomentResponse> buildMomentResponseListWithoutExercises() {

        List<TitleResponse> titleResponses = new ArrayList<TitleResponse>();

        titleResponses.add(TitleResponse.builder().language("en").name("Parts of the body").build());
        titleResponses.add(TitleResponse.builder().language("es").name("Partes del cuerpo").build());
        titleResponses.add(TitleResponse.builder().language("fr").name("Parties du corps").build());
        titleResponses.add(TitleResponse.builder().language("it").name("Parti del corpo").build());
        titleResponses.add(TitleResponse.builder().language("ru").name("Части тела").build());
        titleResponses.add(TitleResponse.builder().language("de").name("Körperteile").build());
        titleResponses.add(TitleResponse.builder().language("pt").name("Partes do corpo").build());

        List<MomentResponse> momentResponses = new ArrayList<>();

        momentResponses.add(MomentResponse.builder()
                .id("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponses)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .status(MomentStatusCategories.NEW.getStatus())
                .exercises(null)
                .build());

        return momentResponses;
    }

    public static List<Moment> buildMomentResponsesRepositoryWithoutExercises() {

        List<Moment> momentResponseRepositories = new ArrayList<>();

        List<Title> titleResponseRepositories = new ArrayList<Title>();
        titleResponseRepositories.add(Title.builder().language("en").name("Parts of the body").build());
        titleResponseRepositories.add(Title.builder().language("es").name("Partes del cuerpo").build());
        titleResponseRepositories.add(Title.builder().language("fr").name("Parties du corps").build());
        titleResponseRepositories.add(Title.builder().language("it").name("Parti del corpo").build());
        titleResponseRepositories.add(Title.builder().language("ru").name("Части тела").build());
        titleResponseRepositories.add(Title.builder().language("de").name("Körperteile").build());
        titleResponseRepositories.add(Title.builder().language("pt").name("Partes do corpo").build());

        momentResponseRepositories.add(Moment.builder()
                .id("zztop")
                .uuid("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponseRepositories)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .exercises(null)
                .build());

        return momentResponseRepositories;
    }

    public static Moment buildMomentWithoutExercises() {

        Moment moment;

        List<Title> titleResponseRepositories = new ArrayList<Title>();
        titleResponseRepositories.add(Title.builder().language("en").name("Parts of the body").build());
        titleResponseRepositories.add(Title.builder().language("es").name("Partes del cuerpo").build());
        titleResponseRepositories.add(Title.builder().language("fr").name("Parties du corps").build());
        titleResponseRepositories.add(Title.builder().language("it").name("Parti del corpo").build());
        titleResponseRepositories.add(Title.builder().language("ru").name("Части тела").build());
        titleResponseRepositories.add(Title.builder().language("de").name("Körperteile").build());
        titleResponseRepositories.add(Title.builder().language("pt").name("Partes do corpo").build());

        moment = Moment.builder()
                .id("zztop")
                .uuid("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponseRepositories)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .exercises(null)
                .build();

        return moment;
    }

    public static List<Moment> buildMomentResponsesRepositoryWithExercises() {

        List<Moment> momentResponseRepositories = new ArrayList<>();
        List<Exercise> exerciseResponseRepositories = new ArrayList<>();
        List<Item> itemResponseRepositories = new ArrayList<>();
        List<Result> resultResponseRespositories = new ArrayList<>();

        itemResponseRepositories.add(Item.builder()
                .uuid("64e984ab-a40b-4522-8102-f9037c7cc130")
                .type("image")
                .audio("head")
                .value("head")
                .role("question")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .type("text")
                .value("Head")
                .role("answer")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("bdd68d0b-d51b-48c2-92b3-d069fa99767d")
                .type("text")
                .value("Shoulders")
                .role("answer")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("e5f82edb-3b60-4f33-a313-b4a6ed3f4eb5")
                .type("text")
                .value("Knees")
                .role("answer")
                .build());

        resultResponseRespositories.add(Result.builder()
                .uuid("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .build());

        exerciseResponseRepositories.add(Exercise.builder()
                .uuid("76240ef0-e29d-4783-a6cf-0140f7e93609")
                .type("imageto3text")
                .items(itemResponseRepositories)
                .results(resultResponseRespositories)
                .build());

        List<Title> titleResponseRepositories = new ArrayList<Title>();
        titleResponseRepositories.add(Title.builder().language("en").name("Parts of the body").build());
        titleResponseRepositories.add(Title.builder().language("es").name("Partes del cuerpo").build());
        titleResponseRepositories.add(Title.builder().language("fr").name("Parties du corps").build());
        titleResponseRepositories.add(Title.builder().language("it").name("Parti del corpo").build());
        titleResponseRepositories.add(Title.builder().language("ru").name("Части тела").build());
        titleResponseRepositories.add(Title.builder().language("de").name("Körperteile").build());
        titleResponseRepositories.add(Title.builder().language("pt").name("Partes do corpo").build());

        momentResponseRepositories.add(Moment.builder()
                .id("moment14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .uuid("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponseRepositories)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .exercises(exerciseResponseRepositories)
                .build());

        return momentResponseRepositories;
    }

    public static Moment buildMomentWithExercises() {

        Moment moment;
        List<Exercise> exerciseResponseRepositories = new ArrayList<>();
        List<Item> itemResponseRepositories = new ArrayList<>();
        List<Result> resultResponseRespositories = new ArrayList<>();

        itemResponseRepositories.add(Item.builder()
                .uuid("64e984ab-a40b-4522-8102-f9037c7cc130")
                .type("image")
                .audio("head")
                .value("head")
                .role("question")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .type("text")
                .value("Head")
                .role("answer")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("bdd68d0b-d51b-48c2-92b3-d069fa99767d")
                .type("text")
                .value("Shoulders")
                .role("answer")
                .build());

        itemResponseRepositories.add(Item.builder()
                .uuid("e5f82edb-3b60-4f33-a313-b4a6ed3f4eb5")
                .type("text")
                .value("Knees")
                .role("answer")
                .build());

        resultResponseRespositories.add(Result.builder()
                .uuid("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .build());

        exerciseResponseRepositories.add(Exercise.builder()
                .uuid("76240ef0-e29d-4783-a6cf-0140f7e93609")
                .type("imageto3text")
                .items(itemResponseRepositories)
                .results(resultResponseRespositories)
                .build());

        List<Title> titleResponseRepositories = new ArrayList<Title>();
        titleResponseRepositories.add(Title.builder().language("en").name("Parts of the body").build());
        titleResponseRepositories.add(Title.builder().language("es").name("Partes del cuerpo").build());
        titleResponseRepositories.add(Title.builder().language("fr").name("Parties du corps").build());
        titleResponseRepositories.add(Title.builder().language("it").name("Parti del corpo").build());
        titleResponseRepositories.add(Title.builder().language("ru").name("Части тела").build());
        titleResponseRepositories.add(Title.builder().language("de").name("Körperteile").build());
        titleResponseRepositories.add(Title.builder().language("pt").name("Partes do corpo").build());

        moment = Moment.builder()
                .id("moment14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .uuid("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponseRepositories)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .exercises(exerciseResponseRepositories)
                .build();

        return moment;
    }

    public static List<MomentResponse> buildMomentResponsesWithExercises() {

        List<MomentResponse> momentResponses = new ArrayList<>();
        List<ExerciseResponse> exerciseResponses = new ArrayList<>();
        List<ItemResponse> itemResponses = new ArrayList<>();
        List<ResultResponse> resultResponses = new ArrayList<>();

        itemResponses.add(ItemResponse.builder()
                .id("64e984ab-a40b-4522-8102-f9037c7cc130")
                .type("image")
                .audio("head")
                .value("head")
                .role("question")
                .build());

        itemResponses.add(ItemResponse.builder()
                .id("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .type("text")
                .value("Head")
                .role("answer")
                .build());

        itemResponses.add(ItemResponse.builder()
                .id("bdd68d0b-d51b-48c2-92b3-d069fa99767d")
                .type("text")
                .value("Shoulders")
                .role("answer")
                .build());

        itemResponses.add(ItemResponse.builder()
                .id("e5f82edb-3b60-4f33-a313-b4a6ed3f4eb5")
                .type("text")
                .value("Knees")
                .role("answer")
                .build());

        resultResponses.add(ResultResponse.builder()
                .id("7835d7e1-de2d-4c92-bdbc-6f31ec8dbb8e")
                .build());

        exerciseResponses.add(ExerciseResponse.builder()
                .id("76240ef0-e29d-4783-a6cf-0140f7e93609")
                .type("imageto3text")
                .items(itemResponses)
                .results(resultResponses)
                .build());

        List<TitleResponse> titleResponses = new ArrayList<TitleResponse>();

        titleResponses.add(TitleResponse.builder().language("en").name("Parts of the body").build());
        titleResponses.add(TitleResponse.builder().language("es").name("Partes del cuerpo").build());
        titleResponses.add(TitleResponse.builder().language("fr").name("Parties du corps").build());
        titleResponses.add(TitleResponse.builder().language("it").name("Parti del corpo").build());
        titleResponses.add(TitleResponse.builder().language("ru").name("Части тела").build());
        titleResponses.add(TitleResponse.builder().language("de").name("Körperteile").build());
        titleResponses.add(TitleResponse.builder().language("pt").name("Partes do corpo").build());

        momentResponses.add(MomentResponse.builder()
                .id("14c2d019-fabb-4fd8-97cd-a96046cab83f")
                .titles(titleResponses)
                .icon("users-2_man")
                .audio("title_parts_of_the_body")
                .status(MomentStatusCategories.NEW.getStatus())
                .exercises(exerciseResponses)
                .build());

        return momentResponses;
    }

}
