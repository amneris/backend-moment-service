package com.abaenglish.moment.objectmother;

import com.abaenglish.moment.controller.v1.momentprogress.dto.MomentProgressMomentResponse;
import com.abaenglish.moment.domain.momentprogress.MomentProgress;
import com.abaenglish.moment.domain.momentprogress.MomentProgressMoment;

import java.util.ArrayList;
import java.util.List;

public class MomentProgressObjectMother {

    public static List<MomentProgressMomentResponse> buildMomentProgressResponse() {

        List<MomentProgressMomentResponse> momentProgressMomentResponse = new ArrayList<>();

//        momentProgressMomentResponse.add(MomentProgressMomentResponse.builder()
//                .uuid("bdf6bbbb-104f-4911-b648-66e7e61c3879")
//                .order(2)
//                .done(true)
//                .addedDate("2017-03-30")
//                .build());

        momentProgressMomentResponse.add(MomentProgressMomentResponse.builder()
                .id("cdf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(3)
                .addedDate("2017-03-30")
                .build());

        momentProgressMomentResponse.add(MomentProgressMomentResponse.builder()
                .id("adf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(1)
                .addedDate("2017-03-30")
                .build());

        return momentProgressMomentResponse;
    }

    public static List<MomentProgressMomentResponse> buildMomentProgressWithoutExpandResponse() {

        List<MomentProgressMomentResponse> momentProgressMomentResponse = new ArrayList<>();

        momentProgressMomentResponse.add(MomentProgressMomentResponse.builder()
                .id("cdf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(3)
                .addedDate("2017-03-30")
                .build());

        return momentProgressMomentResponse;
    }

    public static MomentProgress buildMomentProgress() {

        MomentProgress momentProgress;

        List<MomentProgressMoment> momentProgressMoments = new ArrayList<>();

        momentProgressMoments.add(MomentProgressMoment.builder()
                .uuid("adf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(1)
                .done(false)
                .addedDate("2017-03-30")
                .build());

        momentProgressMoments.add(MomentProgressMoment.builder()
                .uuid("bdf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(2)
                .done(true)
                .addedDate("2017-03-30")
                .build());

        momentProgressMoments.add(MomentProgressMoment.builder()
                .uuid("cdf6bbbb-104f-4911-b648-66e7e61c3879")
                .order(3)
                .done(false)
                .addedDate("2017-03-30")
                .build());

        momentProgress = MomentProgress.builder()
                .id("10000063")
                .uuid("10000063")
                .type("moment_progress")
                .userId("10000063")
                .moments(momentProgressMoments)
                .build();

        return momentProgress;
    }

}
