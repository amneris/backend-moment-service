# Backend Moment Service

This microservice manages moments

**Table of Contens**

<!-- TOC depthFrom:2 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

-[Endpoints](#endpoints)  
-[Assets](#assests)

<!-- /TOC -->


## Endpoints
We have 4 endpoints:

* {{url}}/api/v1/moments/UUID?expand=exercises,results 
    With this endpoint we can obtain the object moment or moment with exercises and results, depending if expand with "exercises" is used or "results".  
    UUID must be informed.

* {{url}}/api/v1/moments?expand=exercises,results 
    Obtain all moments with or without exercises and results depending from expand.

* {{url}}/api/v1/moments/progress 
    POST with param id
    Converts an available user moment to done

* {{url}}/api/v1/moments/progress
    GET last available user moment

  {{url}}/api/v1/moments/progress?expand=all
    GET all availables user moments

DEPRECATED

* {{url}}/api/vabawebapps/moments/progress/USERUUID
    GET last available ABA user moment without security

* {{url}}/api/vabawebapps/moments/progress/USERUUID?expand=all
    GET all availables ABA user moments without security



## Assests

We have two types of assets, audios and images, both are in the same server S3, url:  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land", depending from kind of asset, audio or image, we have another level  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/audio/"  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/image"  
now we need to add the UID corresponding to a moment from we want to obtain something.  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/audio/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0"
"01fb39eb-e3bd-4e37-9f72-c62dcb8189d0" = UID moment corresponding to seasons/hollidays.

Here we almost can obtain all assests.
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/image/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0/autumnwww.png"

For achievents icons from IOS and ANDROID we need to go deeper:  

Android  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/image/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0/android" + "hdpi, mdpi, xhdpi, xxhdpi, xxxhdpi"  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/image/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0/android/xhdpi/holidays_pumpkin.png"  

IOS  
"https://s3-eu-west-1.amazonaws.com/moments.aba.land/image/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0/iOS" + assest  
https://s3-eu-west-1.amazonaws.com/moments.aba.land/image/01fb39eb-e3bd-4e37-9f72-c62dcb8189d0/iOS/holidays_pumpkin.png
