# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Change

### Add
- Add end-point for inject new aba moment

### Remove


## Release

## [1.0.0]
### Add
- Initial release
- User Progress
- Add type & order fields in moment progress
- Rename files
- Implement titles translations
- Save Finished User Moment Progress
- Add done to moment progress
- Add momentType to moment
- Add Available moments end-point  
- MomentProgress dto
- Add security
- Change error codes
- Added progressaba endpoind without security
- Rename progress endpoints names 
- Create obsolete endpoint version of abawebapps
- Change moment progress response
- Added New response to status in moments end-point
- Return last not finalized moment

## [1.0.1]
### Add
- Remove to be premium to obtain moments

## [1.0.2]
### Add
- Change response 403 to 204 when user he's not in a/b test.

